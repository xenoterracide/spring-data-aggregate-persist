package com.xenoterracide;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Foo extends AbstractPersistable<Long> {

	@OneToOne( cascade = CascadeType.ALL )
	@JoinColumn( nullable = false )
	private Bar bar;

	Foo() {
	}

	public Foo( final Bar bar ) {
		this.bar = bar;
	}

	public Bar getBar() {
		return bar;
	}
}
