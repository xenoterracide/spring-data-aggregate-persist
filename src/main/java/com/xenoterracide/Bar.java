package com.xenoterracide;


import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Bar extends AbstractPersistable<Long> {

    @Column
    private String baz;

    Bar() {
    }

    public Bar( final String baz ) {
        this.baz = baz;
    }

    public String getBaz() {
        return baz;
    }
}
