package com.xenoterracide;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@SpringBootApplication
public class Application {
	private static final Logger log = LoggerFactory.getLogger(Application.class);
	public static void main( final String[] args ) {
		ConfigurableApplicationContext context = SpringApplication.run( Application.class );
		FooRepo repo = context.getBean( FooRepo.class );

		repo.save( new Foo( new Bar( "baz0" ) ) );
		repo.save( new Foo( new Bar( "baz1" ) ) );
		repo.save( new Foo( new Bar( "az0" ) ) );
		repo.save( new Foo( new Bar( "2baz2bfoo" ) ) );

		Page<Foo> foos;
		Pageable pageable =  new PageRequest( 0, 2 );
		do {
			 foos = repo.findAllByBarBazContaining( "baz", pageable );
			log.info( "Total: {}", foos.getTotalElements() );

			foos.forEach( ( foo ) -> {
				log.info( "{}", foo );
				log.info( "{}", foo.getBar() );
			} );

			pageable = foos.nextPageable();
		}
		while ( ! foos.isLast() );

        log.info( "count: {}", repo.findAllCustom( pageable ).getTotalElements() );

		context.close();
	}
}
