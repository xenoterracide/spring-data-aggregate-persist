package com.xenoterracide;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface FooRepo extends PagingAndSortingRepository<Foo, Long> {

    @Query( "select f from Foo f" )
    Page<Foo> findAllCustom( Pageable pageable );

    @Query( "from Foo f" )
    Page<Foo> findAllCustomShort( Pageable pageable );

    @Query( "from Foo f order by id DESC" )
    Page<Foo> findAllCustomOrdered( Pageable pageable );

    Page<Foo> findAllByBarBazContaining( String baz, Pageable pageable );
}
