package com.xenoterracide;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith( SpringJUnit4ClassRunner.class )
@SpringApplicationConfiguration( classes = TestApplication.class )
public class FooRepoTest {

    private Pageable pageable = new PageRequest( 0, 2 );

    @Autowired
    private FooRepo fooRepo;

    @Before
    public void setup() {
        fooRepo.save( new Foo( new Bar( "baz0" ) ) );
        fooRepo.save( new Foo( new Bar( "baz1" ) ) );
        fooRepo.save( new Foo( new Bar( "az0" ) ) );
        fooRepo.save( new Foo( new Bar( "2baz2bfoo" ) ) );
    }

    @Test
    public void testFindAllCustom() throws Exception {
        Page<Foo> allCustom = fooRepo.findAllCustom( pageable );

        assertThat( allCustom.getSize(), is( 2 ) );

        Page<Foo> sortByBazAsc = fooRepo.findAllCustom( new PageRequest( 0, 2, Sort.Direction.ASC, "bar.baz" ) );

        assertThat( sortByBazAsc.iterator().next().getBar().getBaz(), is( "2baz2bfoo" ) );

        Page<Foo> complexSort = fooRepo.findAllCustom( new PageRequest( 0, 2, new Sort(
                new Sort.Order( Sort.Direction.DESC, "bar.baz" ),
                new Sort.Order( Sort.Direction.ASC, "id" )
        ) ) );

        assertThat( complexSort.iterator().next().getBar().getBaz(), is( "baz1" ) );
    }

    @Test
    public void testFindAllCustomBroken() throws Exception {
        Page<Foo> allCustom = fooRepo.findAllCustomShort( pageable );

        assertThat( allCustom.getSize(), is( 2 ) );
    }

    @Test
    public void testFindAllCustomOrdered() throws Exception {
        Page<Foo> allCustom = fooRepo.findAllCustomOrdered( pageable );

        assertThat( allCustom.getSize(), is( 2 ) );
    }
}
